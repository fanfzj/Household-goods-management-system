<?php
namespace app\index\model;

use think\Model;

class CategoryModel extends Model
{
    //字段
    public $viewFields=array(
        'category'=>array('cid','cname','pid','create_time','upgrade_time','is_state','is_delete'),
        'category_parent'=>array('cname','_on'=>'category.pid=category_parent.cid')
    );

    // 定义关联模型列表
    protected static $relationModel = [
        // 给关联模型设置数据表
        'Goods'   =>  'hgms_category',
    ];
    // 定义关联外键
    protected $fk = 'cid';
    protected $mapFields = [
        // 为混淆字段定义映射
        'goods_cid'        =>  'Goods.cid',
        'cid' =>  'Category.id',
    ];


    protected $autoWriteTimestamp = true;
    //定义时间戳字段名
    protected $createTime='create_time';
    protected $updateTime='upgrade_time';
    //主键
    protected $pk = 'cid';
    // 设置当前模型对应的完整数据表名称
    protected $table = 'hgms_category';
    //自定义初始化
//    protected function initialize()
//    {
//        //需要调用`Model`的`initialize`方法
//        parent::initialize();
//        //TODO:自定义的初始化
//    }
    // 新增或修改数据
    public function addUpdate($where,$data){
        if (empty($where['cid'])){
            $res=$this::save($data);//必须使用save 使用insert   create_time不会自动写入
        }else{
            $res=$this::update($data,$where);//自动写入更新时间 update_time
        }
        return $res;
    }
    // 传递is_delete和is_state获取对应数据
    public function getDataByState($is_delete=0,$is_state=1){
        $where=array(
            'is_delete'=>$is_delete,
            'is_state'=>$is_state,
        );
        return $this->where($where)->order('sort')->select();
    }

    // 传递cid获取单条数据
    public function getDataByCid($cid){
        return $this->where(array('cid'=>$cid))->find();
    }
    // 传递pid获取下属所有数据
    public function getDataByPid($pid){
        return $this->hasMany('hgms_category','pid','cid');
    }
    public function del($cid){
        $res=$this->where(array('cid'=>$cid))->find();
        $data=array('is_delete'=>1);
        $res->save($data);
        return $res;
    }
}