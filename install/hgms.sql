# Host: 47.104.82.16  (Version: 5.1.73-log)
# Date: 2020-02-04 21:01:14
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES gb2312 */;

#
# Structure for table "hgms_category"
#

DROP TABLE IF EXISTS `hgms_category`;
CREATE TABLE `hgms_category` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `upgrade_time` datetime DEFAULT NULL,
  `is_state` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cid`),
  KEY `hgms_category_hgms_category_cid_fk` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='分类';

#
# Data for table "hgms_category"
#

/*!40000 ALTER TABLE `hgms_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `hgms_category` ENABLE KEYS */;

#
# Structure for table "hgms_container"
#

DROP TABLE IF EXISTS `hgms_container`;
CREATE TABLE `hgms_container` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `con_name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `upgrade_time` datetime DEFAULT NULL,
  `is_state` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "hgms_container"
#

/*!40000 ALTER TABLE `hgms_container` DISABLE KEYS */;
/*!40000 ALTER TABLE `hgms_container` ENABLE KEYS */;

#
# Structure for table "hgms_goods"
#

DROP TABLE IF EXISTS `hgms_goods`;
CREATE TABLE `hgms_goods` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `upgrade_time` datetime DEFAULT NULL,
  `is_state` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `con_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gid`),
  KEY `hgms_goods_hgms_category_cid_fk` (`cid`),
  KEY `hgms_goods_hgms_container_con_id_fk` (`con_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='商品表';

#
# Data for table "hgms_goods"
#

/*!40000 ALTER TABLE `hgms_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `hgms_goods` ENABLE KEYS */;

#
# Structure for table "hgms_user"
#

DROP TABLE IF EXISTS `hgms_user`;
CREATE TABLE `hgms_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `upgrade_time` datetime DEFAULT NULL,
  `is_state` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='用户表';

#
# Data for table "hgms_user"
#

/*!40000 ALTER TABLE `hgms_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `hgms_user` ENABLE KEYS */;
