<?php
namespace app\index\model;

use think\Model;

class UserModel extends Model
{
    //字段
    public $viewFields=array(
        'user'=>array('uid','username','password','create_time','upgrade_time','is_state','is_delete'),
    );
    protected $autoWriteTimestamp = true;
    //定义时间戳字段名
    protected $createTime='create_time';
    protected $updateTime='upgrade_time';
    //主键
    protected $pk = 'uid';
    // 设置当前模型对应的完整数据表名称
    protected $table = 'hgms_user';
    //自定义初始化
//    protected function initialize()
//    {
//        //需要调用`Model`的`initialize`方法
//        parent::initialize();
//        //TODO:自定义的初始化
//    }
    // 新增或修改数据
    public function addUpdate($where,$data){
        if (empty($where['uid'])){
            $res=$this::save($data);//必须使用save 使用insert   create_time不会自动写入
        }else{
            $res=$this::update($data,$where);//自动写入更新时间 update_time
        }
        return $res;
    }
    // 传递is_delete和is_state获取对应数据
    public function getDataByState($is_delete=0,$is_state=1){
        $where=array(
            'is_delete'=>$is_delete,
            'is_state'=>$is_state,
        );
        return $this->where($where)->order('sort')->select();
    }

    // 传递uid获取单条数据
    public function getDataByUid($uid){
        return $this->where(array('uid'=>$uid))->find();
    }

    public function del($uid){
        $res=$this->where(array('uid'=>$uid))->find();
        $data=array('is_delete'=>1);
        $res->save($data);
        return $res;
    }
}