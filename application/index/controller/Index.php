<?php
namespace app\index\controller;
use app\index\model\GoodsModel;
use app\index\model\UserModel;
use app\index\model\ContainerModel;
use app\index\model\CategoryModel;

class Index extends Base
{
    public function index()
    {
        // 批量赋值
        $this->assign([
            'title'  => '首页',
            'goods_num'=>GoodsModel::count(),//物品数量
            'category_num'=>CategoryModel::count(),//柜子数量
            'container_num'=>ContainerModel::count()
        ]);
        // 模板输出
        return $this->fetch('index');
    }
}
