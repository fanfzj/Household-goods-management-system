<?php


namespace app\index\controller;
use think\Controller;

class Base extends Controller
{
    public function _initialize(){
        $this->assign([
            'system_name'=>'物品管理系统',
            'Copyright'=>"Copyright &copy;  2019-".date("Y",time())." ",
            'author_info'=>"<a href='https://zimu.website' target='_parent'>魔都子沐</a>",
            'version'=>"1.0.0-pre"
        ]);
    }
}