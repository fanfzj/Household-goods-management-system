<?php


namespace app\index\controller;
use app\index\model\GoodsModel;
use app\index\model\UserModel;
use app\index\model\ContainerModel;
use app\index\model\CategoryModel;
use \think\Request;
use \think\Db;

class Category extends Base
{
    public function index()
    {
        // 或者批量赋值
        $this->assign([
            'title'  => '物品列表',
            'system_name'=>$this->system_name
        ]);
        // 模板输出
        return $this->fetch('index');
    }
    public function getlist(){
        $request = Request::instance();
        $order=isset($request->param()['order'])?$request->param()['order']:'asc';

        $category_list=Db::table('hgms_category')->where('is_delete',0)->order('cid',$order)->select();
        echo json_encode($category_list,true);
    }


    /**
     * 模糊查询资源
     *
     * @return \think\Response
     */
    public function find(){

    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}