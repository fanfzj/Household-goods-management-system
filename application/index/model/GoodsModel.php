<?php


namespace app\index\model;

use think\Model;

class GoodsModel extends Model
{
    //字段
    public $viewFields=array(
        'goods'=>array('gid','cid','con_id','gname','create_time','upgrade_time','is_state','is_delete'),
        'container'=>array('con_name','_on'=>'goods.con_id=container.con_id'),
        'category'=>array('cname','_on'=>'goods.cid=category.cid')
    );

    protected $autoWriteTimestamp = true;
    //定义时间戳字段名
    protected $createTime='create_time';
    protected $updateTime='upgrade_time';
    //主键
    protected $pk = 'gid';
    // 设置当前模型对应的完整数据表名称
    protected $table = 'hgms_goods';
    //自定义初始化
//    protected function initialize()
//    {
//        //需要调用`Model`的`initialize`方法
//        parent::initialize();
//        //TODO:自定义的初始化
//    }
    // 新增或修改数据
    public function addUpdate($where,$data){
        if (empty($where['gid'])){
            $res=$this::save($data);//必须使用save 使用insert   create_time不会自动写入
        }else{
            $res=$this::update($data,$where);//自动写入更新时间 update_time
        }
        return $res;
    }
    // 传递is_delete和is_state获取对应数据
    public function getDataByState($is_delete=0,$is_state=1){
        $where=array(
            'is_delete'=>$is_delete,
            'is_state'=>$is_state,
        );
        return $this->where($where)->order('sort')->select();
    }

    // 传递gid获取单条数据
    public function getDataByGid($gid){
        return $this->where(array('gid'=>$gid))->find();
    }

    public function del($gid){
        $res=$this->where(array('gid'=>$gid))->find();
        $data=array('is_delete'=>1);
        $res->save($data);
        return $res;
    }
}