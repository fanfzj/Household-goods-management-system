<?php
use think\Route;
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 注册路由到index模块的News控制器的read操作
return [
    '/'=>['index/index',['method' => 'GET|POST']],
    // 路由分组
    '[goods]'     => [
        ''   => ['goods/index', ['method' => 'get']],
        'find' => ['goods/find', ['method' => 'post']],
    ],
    '[category]'     => [
        ''   => ['category/index', ['method' => 'get']],
        'getlist' => ['catgeory/list', ['method' => 'get']],
    ],
];
